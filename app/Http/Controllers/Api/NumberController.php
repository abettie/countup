<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Number;
use App\Events\MessageReceived;
use Illuminate\Support\Facades\Log;

class NumberController extends Controller
{
    /**
     * 全てのナンバーを降順で返す
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Number::orderBy("id", "desc")->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "number" => "bail|required|min:1|max:75|unique:numbers",
        ]);

        $number = new Number();
        $number->setAttribute("number", $request->number);
        $number->save();

        event(new MessageReceived());

        return response()->json(['result' => true]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Number  $number
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Number $number)
    {
        $request->validate([
            "number" => "bail|required|min:1|max:75|unique:numbers",
        ]);

        $number->setAttribute("number", $request->number);
        $number->save();
//        Log::debug(__FILE__.':'.__LINE__." OK!");

        event(new MessageReceived());

        return response()->json(['result' => true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Number  $number
     * @return \Illuminate\Http\Response
     */
    public function destroy(Number $number)
    {
        $number->delete();

        event(new MessageReceived());

        return response()->json(['result' => true]);
    }
}
