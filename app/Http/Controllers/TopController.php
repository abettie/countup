<?php

namespace App\Http\Controllers;

use App\Number;
use Illuminate\Http\Request;

class TopController extends Controller
{
    /**
     * ゲスト側トップページ
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("number.index");
    }
}
